#!/usr/bin/perl

# The begin block is used to insure our environment is set up correctly.
BEGIN {
  use English;
  use Config;
  use Getopt::Long;
  use File::Basename;
  use Data::Dumper;
  
  $Config{'useithreads'} or die "Recompile Perl with threads to run the RTAG Hub.";
  
  # Shared outside of the BEGIN block.
  our $VERSION = "RT AcctGen force queue 1.00";

  our $accountConfigFile = '';
  our $serviceConfigFile = '';
  our $datasourceConfigFile = '';

  # Determine the config files and RTAG home directory.
  #
  #  RTAG_HOME is always the ENV var.
  #
  #  The config files may be given as an option on the command line
  #    or must be the default in RTAG_HOME.
  if (!$ENV{'RTAG_HOME'} && -e '/etc/rtag.conf') {
    open RTAGCONF, '/etc/rtag.conf' or die "Couldn't open /etc/rtag.conf: $!";
    $ENV{'RTAG_HOME'} = <RTAGCONF>;
    chomp $ENV{'RTAG_HOME'};
    close RTAGCONF;
  }
  
  die ("RTAG_HOME is not set.") unless ($ENV{'RTAG_HOME'});
  die ("Missing lib directory in $ENV{'RTAG_HOME'}.") unless (-e $ENV{'RTAG_HOME'} . '/lib');
  
  # Add the local lib directory to our INC array.
  push(@INC, $ENV{'RTAG_HOME'} . '/lib');
  
  # There are three configuration files:
  #
  #    accounts.xml - specify attributes related to each account
  #    services.xml - specify connection and worker settings
  #    datasources.xml - specify database connection settings
  #
  # The two files may be used in various combinations for maximum
  # flexibility, such as testing new account configuration using
  # production database configurations.  It also makes it easier
  # to separate changes from production connectivity settings.
  $accountConfigFile = $ENV{'RTAG_HOME'} . '/conf/accounts.xml' unless ($accountConfigFile);
  die ("Missing account config file $accountConfigFile.") unless (-e $accountConfigFile);
  
  $serviceConfigFile = $ENV{'RTAG_HOME'} . '/conf/services.xml' unless ($serviceConfigFile);
  die ("Missing datasource config file $serviceConfigFile.") unless (-e $serviceConfigFile);
  
  $datasourceConfigFile = $ENV{'RTAG_HOME'} . '/conf/datasources.xml' unless ($datasourceConfigFile);
  die ("Missing datasource config file $datasourceConfigFile.") unless (-e $datasourceConfigFile);
  
}

use strict;
use warnings;

use LWP::UserAgent;
use JSON;
use Data::Dumper;

use XML::Simple;
use Mail::Sendmail;

our($accountConfigFile, $serviceConfigFile, $datasourceConfigFile);

our $services = XMLin(
            $serviceConfigFile, 
            'ForceArray' => [ qw ( 
                        service
                      ) ], 
            'KeyAttr' => {
                    'service' => 'name'
                  },
          )->{'service'};

our $otfConfig = XMLin(
            $serviceConfigFile, 
            'ForceArray' => [ qw ( 
                      ) ], 
            'KeyAttr' => {
                    'service' => 'name'
                  },
          )->{'service'}{'opentwofactor'};

my $wsHost = $otfConfig->{'tfaws_host'};
my $wsUser = $otfConfig->{'tfaws_user'};
my $wsPass = $otfConfig->{'tfaws_pass'};

my $ua = LWP::UserAgent->new();

my $request = HTTP::Request->new('POST', $wsHost . '/authentication/v1');
my $data = {
    'username' => $wsUser,
    'password' => $wsPass,
    'type' => 'usernamePassword'
};

$request->content(to_json($data));
$request->header('Content-Type', 'application/json');

my $response = $ua->simple_request($request);

my $tokenInfo = extractResponseData('response' => $response, 'request' => $request);

my $token = $tokenInfo->{'token'};

$request = HTTP::Request->new('GET', $wsHost . '/config/v1/TwoFactor?category=Opt-in%20Check&format=list&token=' . $token);
$response = $ua->simple_request($request);

my $config = extractResponseData('response' => $response, 'request' => $request);

# Decorate the data from ConfigMgr with locally provided config
$config->{'smtpServer'} = $otfConfig->{'tfacheck_smtp'} || 'mailfwd.miamioh.edu';
$config->{'overrideAddress'} = $otfConfig->{'tfacheck_override'} || '';

$request = HTTP::Request->new('GET', $wsHost . '/twoFactor/v1/group?token=' . $token);
$response = $ua->simple_request($request);

my $groups = extractResponseData('response' => $response, 'request' => $request);

unless (ref($groups) eq 'ARRAY') {
  exitWithError('message' => 'Group response data element is not an array', 'request' => $request, 'response' => $response);
}

foreach my $group (@{$groups}) {
  $request = HTTP::Request->new('GET', $wsHost . '/twoFactor/v1/contact?id=' . $group->{'id'} . '&token=' . $token);
  $response = $ua->simple_request($request);

  my $contacts = extractResponseData('response' => $response, 'request' => $request);

  unless (ref($contacts) eq 'ARRAY') {
    exitWithError('message' => 'Contact response data element is not an array', 'request' => $request, 'response' => $response);
  }

  $request = HTTP::Request->new('GET', $wsHost . '/twoFactor/v1/member?id=' . $group->{'id'} . '&token=' . $token);
  $response = $ua->simple_request($request);

  my $members = extractResponseData('response' => $response, 'request' => $request);

  unless (ref($members) eq 'ARRAY') {
    exitWithError('message' => 'Member response data element is not an array', 'request' => $request, 'response' => $response);
  }

  foreach my $member (@{$members}) {
    
    if ($member->{'enrolled'} eq 'false') {
      sendNotice(
          'contacts' => $contacts,
          'group' => $group,
          'member' => $member,
          'action' => 'enroll',
          'config' => $config,
        );
    }

    if ($member->{'enrolled'} eq 'true' && $member->{'optedIn'} eq 'false') {
      sendNotice(
          'contacts' => $contacts,
          'group' => $group,
          'member' => $member,
          'action' => 'opt-in',
          'config' => $config,
        );
    }

  }
}

sub extractResponseData {
  my $params = { @_ };

  my $request = $params->{'request'};
  my $response = $params->{'response'};

  unless ($response->code == 200) {
    exitWithError('message' => 'Request for group service failed', 'request' => $request, 'response' => $response);
  }

  my $payload = from_json(${$response->content_ref()});

  unless (defined($payload->{'data'})) {
    exitWithError('message' => 'Group response contains no data element', 'request' => $request, 'response' => $response);

  }

  return $payload->{'data'};
}

sub sendNotice {
  my $params = { @_ };

  my $contacts = $params->{'contacts'};
  my $group = $params->{'group'};
  my $member = $params->{'member'};
  my $requiredAction = $params->{'action'};
  my $config = $params->{'config'};

  my(@contactList, @contactEmailList);
  foreach my $contact (@{$contacts}) {
    push(@contactList, "$contact->{'firstName'} $contact->{'lastName'}");
    push(@contactEmailList, $contact->{'emailAddress'});
  }

  my $data = {
    'contactList' => join(', ', @contactList),
    'firstName' => $member->{'firstName'},
    'lastName' => $member->{'lastName'},
    'emailAddress' => $member->{'emailAddress'},
    'serviceText' => $group->{'serviceText'},
    'rationaleText' => $group->{'rationaleText'},
    'helpText' => $group->{'helpText'},
    'action' => $requiredAction,
  };

  logEntry('message' => "Need to tell $data->{'contactList'} that $member->{'emailAddress'} must $requiredAction for $group->{'serviceText'}");
  my $message = $config->{'messageTemplate'};
  
  foreach my $key (keys %{$data}) {
    $message =~ s/\{$key\}/$data->{$key}/g;
  }

  $message =~ s/\n{2,}/\n\n/g;
  $message =~ s/^\n+//;
  $message =~ s/\n+$//;

  unless ($message) {
    exitWithError('No message resulted from the template and data');
  }

  my %mail = (
          'smtp' => $config->{'smtpServer'},
          'from' => $config->{'messageFrom'},
          'subject' => $config->{'messageSubject'},
          'cc' => join(', ', @contactEmailList),
      );

  if (defined($config->{'overrideAddress'}) && ref($config->{'overrideAddress'}) eq '' &&
      $config->{'overrideAddress'} =~ /.*@.*/) {
    $mail{'to'} = $config->{'overrideAddress'};
  } else {
    $mail{'to'} = $member->{'emailAddress'};
  }
  
  $mail{'body'} = $message;

  if (sendmail %mail) { logEntry('message' => "Notification mail sent OK.") }
      else { logEntry('message' => "Error sending mail: $Mail::Sendmail::error") }
} 

sub exitWithError {
  my $params = { @_ };

  my $message = $params->{'message'} || 'An unknown error occurred';

  print "$message\n";
  print $request->as_string() if ($params->{'request'});
  print $response->as_string() if ($params->{'response'});
  logEntry('message' => $message);
  logEntry('message' => $request->as_string()) if ($params->{'request'});
  logEntry('message' => $response->as_string()) if ($params->{'response'});
  exit 1;
}

sub logEntry {
  my $params = { @_ };

  my $message = $params->{'message'} || '';

  if ($message) {
    my $time = scalar localtime();
    #print "[$time] $message\n";
    open LOG, ">>check.log" or die "Couldn't open check.log: $!";
    print LOG "[$time] $message\n";
    close LOG;
  }
}
