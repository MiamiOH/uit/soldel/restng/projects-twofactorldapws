<?php

namespace MiamiOH\TwoFactorLdapWs\Resources;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Util\ResourceProvider;

class TwoFactorResourceProvider extends ResourceProvider
{
    public function registerDefinitions(): void
    {

    }

    public function registerServices(): void
    {
        $this->addService(array(
            'name' => 'TwoFactor',
            'class' => 'MiamiOH\TwoFactorLdapWs\Services\TwoFactor',
            'description' => 'Provides Two Factor data services for users',
            'set' => array(
                'ldap' => array('type' => 'service', 'name' => 'APILDAPFactory'),
            ),
        ));

        $this->addService(array(
            'name' => 'TwoFactorGroup',
            'class' => 'MiamiOH\TwoFactorLdapWs\Services\Group',
            'description' => 'Provides Two Factor related group services',
            'set' => array(
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
            ),

        ));

        $this->addService(array(
            'name' => 'TwoFactorDirectory',
            'class' => 'MiamiOH\TwoFactorLdapWs\Services\Directory',
            'description' => 'Provides Two Factor related directory services',
            'set' => array(
                'ldap' => array('type' => 'service', 'name' => 'APILDAPFactory'),
            ),
        ));

        $this->addService(array(
            'name' => 'TwoFactorContact',
            'class' => 'MiamiOH\TwoFactorLdapWs\Services\Contact',
            'description' => 'Provides Two Factor related contact services',
            'set' => array(
                'directory' => array('type' => 'service', 'name' => 'TwoFactorDirectory'),
                'database' => array('type' => 'service', 'name' => 'APIDatabaseFactory'),
            ),
        ));

        $this->addService(array(
            'name' => 'TwoFactorMember',
            'class' => 'MiamiOH\TwoFactorLdapWs\Services\Member',
            'description' => 'Provides Two Factor related member services',
            'set' => array(
                'directory' => array('type' => 'service', 'name' => 'TwoFactorDirectory'),
            ),
        ));

    }

    public function registerResources(): void
    {
        $this->addResource(array(
            'action' => 'update',
            'name' => 'twoFactor.v1.user.updateid',
            'description' => 'Update users Status of TFA',
            'pattern' => '/twoFactor/v1/user/:id',
            'service' => 'TwoFactor',
            'method' => 'updateLDAP',
            'returnType' => 'none',
            'params' => array(
                'id' => array('description' => 'A uniqueID'),
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'TFA Web Services',
                    'module' => 'User',
                    'key' => 'Update'),
            ),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'twoFactor.v1.user.id',
            'description' => 'Get users Status of TFA',
            'pattern' => '/twoFactor/v1/user/:id',
            'service' => 'TwoFactor',
            'method' => 'readFromLDAP',
            'returnType' => 'collection',
            'params' => array(
                'id' => array('description' => 'A uniqueID'),
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'TFA Web Services',
                    'module' => 'User',
                    'key' => 'Update'),
            ),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'twoFactor.v1.group',
            'description' => 'Get a collection of groups for TFA opt-in',
            'pattern' => '/twoFactor/v1/group',
            'service' => 'TwoFactorGroup',
            'method' => 'getGroupList',
            'returnType' => 'collection',
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'TFA Web Services',
                    'module' => 'Group',
                    'key' => 'view'),
            ),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'twoFactor.v1.group.id',
            'description' => 'Get a group for TFA opt-in',
            'pattern' => '/twoFactor/v1/group/:id',
            'service' => 'TwoFactorGroup',
            'method' => 'getGroup',
            'returnType' => 'model',
            'params' => array(
                'id' => array('description' => 'A group id'),
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'TFA Web Services',
                    'module' => 'Group',
                    'key' => 'view'),
            ),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'twoFactor.v1.contact',
            'description' => 'Get a collection of contacts for TFA opt-in',
            'pattern' => '/twoFactor/v1/contact',
            'service' => 'TwoFactorContact',
            'method' => 'getContactList',
            'returnType' => 'collection',
            'options' => array(
                'id' => array('required' => true, 'description' => 'A single group id')
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'TFA Web Services',
                    'module' => 'Group',
                    'key' => 'view'),
            ),
        ));

        $this->addResource(array(
            'action' => 'read',
            'name' => 'twoFactor.v1.member',
            'description' => 'Get a collection of members for TFA opt-in',
            'pattern' => '/twoFactor/v1/member',
            'service' => 'TwoFactorMember',
            'method' => 'getMemberList',
            'returnType' => 'collection',
            'options' => array(
                'id' => array('required' => true, 'description' => 'A single group id')
            ),
            'middleware' => array(
                'authenticate' => array(),
                'authorize' => array(
                    'application' => 'TFA Web Services',
                    'module' => 'Group',
                    'key' => 'view'),
            ),
        ));

    }

    public function registerOrmConnections(): void
    {

    }
}