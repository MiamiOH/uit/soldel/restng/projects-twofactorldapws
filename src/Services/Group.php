<?php

namespace MiamiOH\TwoFactorLdapWs\Services;

class Group extends \MiamiOH\RESTng\Service
{

    private $dbDataSourceName = 'TFAWS_DB';
    private $dbh;


    public function setDatabase($db)
    {
        $this->dbh = $db->getHandle($this->dbDataSourceName);
    }

    public function getGroupList()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();

        $records = $this->dbh->queryall_array('
        select group_id as id, group_dn as dn, service_text, rationale_text, help_text
            from tfa_optin_group
      ');

        if ($records === \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET) {
            $records = array();
        }

        for ($i = 0; $i < count($records); $i++) {
            $records[$i] = $this->camelCaseKeys($records[$i]);
        }

        $response->setPayload($records);

        return $response;
    }

    public function getGroup()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $id = $request->getResourceParam('id');

        if (!isset($id)) {
            throw new \Exception('You must provide a group id when calling the group service');
        }

        $record = $this->queryDB(array($id));

        if (isset($record) && $record[$id] == array()) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            return $response;
        }

        if ($record[$id]['dn'] == '') {
            throw new \Exception('Group record for id "' . $id . '" missing DN value');
        }

        $response->setPayload($record[$id]);

        return $response;
    }

    protected function queryDB($groupList)
    {

        $groupsData = array();

        foreach ($groupList as $groupId) {
            $groupData = $this->dbh->queryfirstrow_assoc('
          select group_id as id, group_dn as dn, service_text, rationale_text, help_text
            from tfa_optin_group
            where group_id = ?
        ', $groupId);

            if ($this->dbh->error_string) {
                throw new \Exception('DB error selecting group: ' . $this->dbh->error_string);
            }

            if ($groupData === \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET) {
                $groupData = array();
            }
            $groupsData[$groupId] = $this->camelCaseKeys($groupData);
        }
        return $groupsData;


    }
}
