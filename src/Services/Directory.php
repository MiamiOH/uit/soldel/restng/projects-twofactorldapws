<?php

namespace MiamiOH\TwoFactorLdapWs\Services;

class Directory
{
    private $olDataSourceName = 'TFAWS_OpenLDAP';
    private $adDataSourceName = 'TFAWS_AD';

    private $olSearchBase = 'ou=people,dc=muohio,dc=edu';

    private $ad;
    private $ldap;

    public function setLdap($ldap)
    {
        $this->ad = $ldap->getHandle($this->adDataSourceName);
        $this->ldap = $ldap->getHandle($this->olDataSourceName);
    }

    public function getAdEntry($options = array())
    {
        if (!is_array($options) || !$options) {
            throw new \Exception(__CLASS__ . '::getAdEntry() expects options array');
        }

        $logger = \Logger::getLogger(__CLASS__);

        $group = array();

        if (isset($options['dn'])) {
            list($searchFilter, $baseDn) = explode(',', $options['dn'], 2);

            $logger->debug("filter: $searchFilter, base: $baseDn");

            $this->ad->search($searchFilter, array('cn', 'member'), $baseDn);
            $entry = $this->ad->next_entry();

            if (isset($entry->dn)) {
                $group['dn'] = $entry->dn;
                $group['cn'] = $entry->attributes['cn'];
                $group['member'] = $entry->attributes['member'];
            }
        }

        return $group;
    }

    public function getOpenLDAPEntry($options = array())
    {
        if (!is_array($options) || !$options) {
            throw new \Exception(__CLASS__ . '::getAdEntry() expects options array');
        }

        $logger = \Logger::getLogger(__CLASS__);

        $user = array();

        if (isset($options['uid'])) {
            $this->ldap->search('uid=' . $options['uid'],
                array('uid', 'givenname', 'sn', 'mail', 'muohioedupersontfaenrolled', 'muohioedupersontfaoptedin'),
                $this->olSearchBase);
            $entry = $this->ldap->next_entry();

            $logger->debug(print_r($entry, true));

            if (isset($entry->dn)) {
                $user['uid'] = $entry->attributes['uid'][0];
                $user['firstName'] = isset($entry->attributes['givenname']) ? $entry->attributes['givenname'][0] : '';
                $user['lastName'] = isset($entry->attributes['sn']) ? $entry->attributes['sn'][0] : '';
                $user['emailAddress'] = isset($entry->attributes['mail']) ? $entry->attributes['mail'][0] : '';
                $user['enrolled'] = isset($entry->attributes['muohioedupersontfaenrolled']) ? $entry->attributes['muohioedupersontfaenrolled'][0] : 'false';
                $user['optedIn'] = isset($entry->attributes['muohioedupersontfaoptedin']) ? $entry->attributes['muohioedupersontfaoptedin'][0] : 'false';
            }
        }

        return $user;
    }


}