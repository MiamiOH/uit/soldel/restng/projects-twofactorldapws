<?php

namespace MiamiOH\TwoFactorLdapWs\Services;

class TwoFactor extends \MiamiOH\RESTng\Service
{
    var $attribute_ds = 'TFAWS_OpenLDAP';
    var $dn = '';
    var $uid = '';
    var $container = 'ou=people,dc=muohio,dc=edu';
    var $error = '';
    var $default_search = 'uid';
    var $return_all = false;
    var $attributeNamesArray = array('muohioedupersonTFAEnrolled' => 'enrolled', 'muohioedupersonTFAOptedIn' => 'optedIn');

    // Method to instantiate the LDAP handle
    public function setLdap($ldap)
    {
        $this->myldap = $ldap->getHandle($this->attribute_ds);
    }

    public function getLdap()
    {
        return $this->myldap;
    }

    public function getAttributeList()
    {
        return $this->myResults;
    }

    // GET method to return data from ldap
    public function readFromLDAP()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();
        $id = $request->getResourceParam('id');

        if ($id) {
            $this->queryLDAP($id);
            // Building Return data, this will include the ID and the additional ldap attributes.
            $record = array('id' => $id);
            $record = array_merge($record, $this->returnableAttribute($this->myResults));
            $response->setStatus(\MiamiOH\RESTng\App::API_OK);
            $response->setPayload($record);
        } else {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
        }

        return $response;
    }

    // PUT method -- used to update status of TFA enrollment or optedIn-ness in LDAP record
    public function updateLDAP()
    {
        $response = $this->getResponse();
        $request = $this->getRequest();
        $params = $request->getResourceParams();
        $payload = $request->getData();
        $changes = $this->friendlyToUsableAttributeSwitch($payload);
        $dn = $this->default_search . '=' . $payload['id'] . ',' . $this->container;
        $success = $this->updateLDAPTFA($dn, $changes);

        $response->setStatus(\MiamiOH\RESTng\App::API_OK);
        return $response;
    }


    //////////////////////////////////////////////////////////
    //
    //  PRIVATE METHODS
    //
    //////////////////////////////////////////////////////////

    private function updateLDAPTFA($dn, $requestedChange)
    {
        foreach ($requestedChange as $key => $value) {
            $value = strtolower($value);
            if ($value == 'true') {
                $changes[$key] = 'true';//$value;
            } elseif ($value == 'false') {
                $changes[$key] = array();
            }
        }
        // print_r($changes);
        // print_r($dn);
        $success = ldap_modify($this->myldap->ds, $dn, $changes);
        return $success;
    }


    // Generates the attributes needed for LDAP query based on data in $attributeNamesArray
    private function getAttributes()
    {
        $attributes = array();
        foreach ($this->attributeNamesArray as $values => $key) {
            $attributes[] = $values;
        }
        return $attributes;
    }

    private function friendlyToUsableAttributeSwitch($sourceData)
    {
        $flippedArray = array_flip($this->attributeNamesArray);
        foreach ($sourceData as $key => $value) {
            if (array_key_exists($key, $flippedArray)) {
                $attributeArray[$flippedArray[$key]] = $value;
            }
        }
        return $attributeArray;
    }


    // Method to query records in LDAP
    private function queryLDAP($id)
    {
        $this->myldap->search($this->default_search . '=' . $id, $this->getAttributes(), $this->container);
        $entry =& $this->myldap->next_entry();
        // var_dump($entry);
        // var_dump($entry->attributes);
        $this->attributeNamesArray = array_change_key_case($this->attributeNamesArray, CASE_LOWER);
        foreach ($this->attributeNamesArray as $key => $value) {
            // $key = strtolower($key);
            $this->myResults[$key] = strtolower($entry->attributes[$key][0]) ?: "false";
        }
    }

    // This method changes the LDAP Attributes from technical name to userFriendly one expected in WS
    private function returnableAttribute($sourceData)
    {
        foreach ($sourceData as $key => $value) {
            $this->attributeNamesArray = array_change_key_case($this->attributeNamesArray, CASE_LOWER);
            $attributeArray[$this->attributeNamesArray[strtolower($key)]] = $value;
        }
        return $attributeArray;
    }

}
