<?php

namespace MiamiOH\TwoFactorLdapWs\Services;

class Contact extends \MiamiOH\RESTng\Service
{

    private $dbDataSourceName = 'TFAWS_DB';

    private $dbh;
    private $directory;

    public function setDirectory($directory)
    {
        $this->directory = $directory;
    }

    public function setDatabase($db)
    {
        $this->dbh = $db->getHandle($this->dbDataSourceName);
    }

    public function getContactList()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();

        if (!isset($options['id'])) {
            throw new \Exception('You must provide a group id when calling the contact service');
        }

        $group = $this->callResource('twoFactor.v1.group.id', array('params' => array('id' => $options['id'])))->getPayload();

        if (!(isset($group['id']) && $group['id'] == $options['id'])) {
            throw new \Exception('Group with id "' . $options['id'] . '" was not found');
        }

        if (!(isset($group['dn']) && $group['dn'])) {
            throw new \Exception('Group with id "' . $options['id'] . '" has no dn');
        }

        $groupContacts = $this->dbh->queryall_array('
        select contact_id, contact_uid
          from tfa_optin_group_contact
          where group_id = ?
      ', $group['id']);

        if ($groupContacts === \MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET) {
            $groupContacts = array();
        }

        $records = array();

        foreach ($groupContacts as $contact) {
            $ldapEntry = $this->directory->getOpenLDAPEntry(array('uid' => $contact['contact_uid']));

            $records[] = array(
                'id' => $contact['contact_id'],
                'uid' => $contact['contact_uid'],
                'firstName' => isset($ldapEntry['firstName']) ? $ldapEntry['firstName'] : '',
                'lastName' => isset($ldapEntry['lastName']) ? $ldapEntry['lastName'] : '',
                'emailAddress' => isset($ldapEntry['emailAddress']) ? $ldapEntry['emailAddress'] : '',
            );
        }

        $response->setPayload($records);
        $this->log->debug(print_r($records, true));

        return $response;
    }

}
