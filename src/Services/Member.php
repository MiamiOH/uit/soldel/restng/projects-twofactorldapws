<?php

namespace MiamiOH\TwoFactorLdapWs\Services;

class Member extends \MiamiOH\RESTng\Service
{

    private $directory;

    public function setDirectory($directory)
    {
        $this->directory = $directory;
    }

    public function getMemberList()
    {
        $request = $this->getRequest();
        $response = $this->getResponse();

        $options = $request->getOptions();

        if (!isset($options['id'])) {
            throw new \Exception('You must provide a group id when calling the member service');
        }

        $group = $this->callResource('twoFactor.v1.group.id', array('params' => array('id' => $options['id'])))->getPayload();

        if (!(isset($group['id']) && $group['id'] == $options['id'])) {
            throw new \Exception('Group with id "' . $options['id'] . '" was not found');
        }

        if (!(isset($group['dn']) && $group['dn'])) {
            throw new \Exception('Group with id "' . $options['id'] . '" has no dn');
        }

        $adGroup = $this->directory->getAdEntry(array('dn' => $group['dn']));

        if (!(isset($adGroup['dn']) && strtolower($adGroup['dn']) == strtolower($group['dn']))) {
            $response->setStatus(\MiamiOH\RESTng\App::API_NOTFOUND);
            return $response;
        }

        if (!(isset($adGroup['member']) && is_array($adGroup['member']))) {
            throw new \Exception('AD group "' . $group['dn'] . '" attribute "member" is not an array');
        }

        $records = array();
        foreach ($adGroup['member'] as $memberDn) {
            if (preg_match('/cn=([^,]+)/i', $memberDn, $matches)) {
                $userEntry = $this->directory->getOpenLDAPEntry(array('uid' => $matches[1]));
                $records[] = $userEntry;
            }
        }

        $response->setPayload($records);

        return $response;
    }

}
