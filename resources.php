<?php

return [
    'resources' => [
        'twoFactor' => [
            MiamiOH\TwoFactorLdapWs\Resources\TwoFactorResourceProvider::class,
        ],
    ]
];