# Get information about used to verify opt-in status of users
Feature: Get contacts related to groups for TFA opt-in status
 As a consumer of the TFA contact service
 I want to get information about the contact(s) for each group
 In order to notify them of members who are not opted in

Background:
  Given the test data is ready

Scenario: Require a group id
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /twoFactor/v1/contact
  Then the HTTP status code is 500
 
Scenario: Get a list of contacts
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /twoFactor/v1/contact?id=1
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  And the response data element first entry contains a id key matching "1"
  And the response data element first entry contains a uid key matching "tepeds"
  And the response data element first entry contains a firstName key matching "Dirk"
  And the response data element first entry contains a lastName key matching "Tepe"
  And the response data element first entry contains a emailAddress key matching "tepeds@miamioh.edu"

