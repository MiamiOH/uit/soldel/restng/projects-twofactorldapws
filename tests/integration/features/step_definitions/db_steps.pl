#!perl

use strict;
use warnings;

use Test::More;
use Test::BDD::Cucumber::StepFile;

use DBI;
use Data::Dumper;

use lib 'lib';
use StepConfig;

our $dbh = DBI->connect("DBI:Oracle:XE", "restng", "Hello123") || die DBI->errstr;

our $testRecords = {
    'contact' => {
        'table' => 'tfa_optin_group_contact',
        'records' => [
            {
                'contact_id' => 1,
                'contact_uid' => 'contact1'
            },
            {
                'contact_id' => 2,
                'contact_uid' => 'contact2'
            },
            {
                'contact_id' => 3,
                'contact_uid' => 'contact3'
            },
        ]
    }
};

Given qr/an empty (\S+) table/, sub { 
    my $table = $testRecords->{$1}{'table'};

    $dbh->do(qq{ delete from $table });
};

Given q/the test data is ready/, sub {
    $dbh->do(qq{ delete from tfa_optin_group_contact });
    $dbh->do(qq{ delete from tfa_optin_group });

    $dbh->do(qq{
            insert into tfa_optin_group (group_id, group_dn, service_text, rationale_text, help_text)
                values (1, 'CN=duit-dsttstgrp2,OU=uit,OU=test,OU=manual,OU=Groups,DC=ittst,DC=muohio,DC=edu',
                    'An important system',
                    'This is an important system.',
                    'Call ITHelp')
        });

    $dbh->do(qq{
            insert into tfa_optin_group (group_id, group_dn, service_text, rationale_text, help_text)
                values (2, 'CN=uit-cptest,OU=uit,OU=uit,OU=test,OU=manual,OU=Groups,DC=ittst,DC=muohio,DC=edu',
                    'An CP system',
                    'This is an important CP system.',
                    'Call Campus Partnerships')
        });

    $dbh->do(qq{
            insert into tfa_optin_group_contact (contact_id, group_id, contact_uid)
                values (1, 1, 'tepeds')
        });

    $dbh->do(qq{
            insert into tfa_optin_group_contact (contact_id, group_id, contact_uid)
                values (2, 1, 'kidddw')
        });

    $dbh->do(qq{
            insert into tfa_optin_group_contact (contact_id, group_id, contact_uid)
                values (3, 2, 'henslel')
        });

};

Given qr/a test (\S+) exists for group (\d+)/, sub {
    my $recordType = $1;
    my $groupId = $2;

    my $table = $testRecords->{$recordType}{'table'};
    my $testData = $testRecords->{$recordType}{'records'}[0];

    if ($recordType eq 'contact') {
        $dbh->do(qq{
                insert into $table (contact_id, group_id, contact_uid)
                    values ($testData->{'contact_id'}, $groupId, '$testData->{'contact_uid'}')
            });
    }

};

