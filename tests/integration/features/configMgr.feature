# Use ConfigMgr to get application data
Feature: Make a REST call to get configuration data from ConfigMgr
 As the running application
 I want to make a request to the web service to get configuration data
 In order to externalize user changeable data

Scenario: Get configuration data
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /config/TwoFactor?category=Opt-in%20Check&format=list
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  And the response data element contains a messageTemplate key matching ".*"

