# Get information about used to verify opt-in status of users
Feature: Get members related to groups for TFA opt-in status
 As a consumer of the TFA member service
 I want to get information about the members for each group
 In order to determine if they are opted in

Background:
  Given the test data is ready

Scenario: Require a group id
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /twoFactor/v1/member
  Then the HTTP status code is 500
 
Scenario: Get a list of members
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /twoFactor/v1/member?id=1
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  And the response data element first entry contains a uid key matching ".*"
  And the response data element first entry contains a firstName key matching ".*"
  And the response data element first entry contains a lastName key matching ".*"
  And the response data element first entry contains a emailAddress key matching ".*"
  And the response data element first entry contains a enrolled key matching "(true|false)"
  And the response data element first entry contains a optedIn key matching "(true|false)"
