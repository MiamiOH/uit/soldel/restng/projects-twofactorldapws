# Get information about used to verify opt-in status of users
Feature: Get groups for auditing TFA opt-in status
 As a consumer of the TFA group service
 I want to get information about the groups which require TFA opt-in
 In order to get configuration and members

Background:
  Given the test data is ready

Scenario: Get a list of groups
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /twoFactor/v1/group
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  And the response data element first entry contains a id key matching ".*"
  And the response data element first entry contains a dn key matching ".*"
  And the response data element first entry contains a serviceText key matching ".*"
  And the response data element first entry contains a rationaleText key matching ".*"
  And the response data element first entry contains a helpText key matching ".*"

Scenario: Get a specific group given an id
  Given a REST client
  And a valid token for an authorized user
  When I make a GET request for /twoFactor/v1/group/1
  Then the HTTP status code is 200
  And the HTTP Content-Type header is "application/json"
  And the response data element contains a id key matching ".*"
  And the response data element contains a dn key matching ".*"
  And the response data element contains a serviceText key matching ".*"
  And the response data element contains a rationaleText key matching ".*"
  And the response data element contains a helpText key matching ".*"

