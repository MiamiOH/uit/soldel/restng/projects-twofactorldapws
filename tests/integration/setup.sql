insert into ws_authentication_local_users (username, algorithm, credential_hash, expiration_date)
  values (lower('TFA_WS_USER'), 1, lower(rawtohex(utl_raw.cast_to_raw(dbms_obfuscation_toolkit.md5(input_string=>'MKZf9sKDztO0CNcWuFXG3J0YK5J9THZg2IGnrb5d')))),
      sysdate + 365);

insert into a_application (application_id, name)
  values ((select nvl(max(application_id), 0) + 1 from a_application), 'TFA Web Services');

insert into a_module (module_id, application_id, name) 
  values ((select nvl(max(module_id), 0) + 1 from a_module), (select application_id from a_application where name = 'TFA Web Services'), 'User');

insert into a_module (module_id, application_id, name) 
  values ((select nvl(max(module_id), 0) + 1 from a_module), (select application_id from a_application where name = 'TFA Web Services'), 'Group');

insert into a_entity (entity_id, dn, type, scope, application_id)
  values ((select nvl(max(entity_id), 0) + 1 from a_entity), 'TFA_WS_USER', 'AP', 'A', (select application_id from a_application where name = 'TFA Web Services'));

insert into a_grant_key (grant_key_id, grant_key, scope, application_id)
  values ((select nvl(max(grant_key_id), 0) + 1 from a_grant_key), 'update', 'A', (select application_id from a_application where name = 'TFA Web Services'));

insert into a_grant_key (grant_key_id, grant_key, scope, application_id)
  values ((select nvl(max(grant_key_id), 0) + 1 from a_grant_key), 'view', 'A', (select application_id from a_application where name = 'TFA Web Services'));

insert into a_authorization (authorization_id, entity_id, module_id, grant_by, grant_date, grant_start_date, grant_end_date, grant_key_id, status)
  values ((select nvl(max(authorization_id), 0) + 1 from a_authorization), (select entity_id from a_entity where dn = 'TFA_WS_USER'),
    (select module_id from a_module where name = 'User' and application_id = (
    (select application_id from a_application where name = 'TFA Web Services'))), 'doej', sysdate, sysdate, sysdate + 365, 
    (select grant_key_id from a_grant_key where grant_key = 'update' and application_id = (select application_id from a_application where name = 'TFA Web Services')), 'A');

insert into a_authorization (authorization_id, entity_id, module_id, grant_by, grant_date, grant_start_date, grant_end_date, grant_key_id, status)
  values ((select nvl(max(authorization_id), 0) + 1 from a_authorization), (select entity_id from a_entity where dn = 'TFA_WS_USER'),
    (select module_id from a_module where name = 'Group' and application_id = (
    (select application_id from a_application where name = 'TFA Web Services'))), 'doej', sysdate, sysdate, sysdate + 365, 
    (select grant_key_id from a_grant_key where grant_key = 'view' and application_id = (select application_id from a_application where name = 'TFA Web Services')), 'A');




insert into a_application (application_id, name)
  values ((select nvl(max(application_id), 0) + 1 from a_application), 'CM-TwoFactor');

insert into a_module (module_id, application_id, name) 
  values ((select nvl(max(module_id), 0) + 1 from a_module), (select application_id from a_application where name = 'CM-TwoFactor'), 'Opt-in Check');

insert into a_entity (entity_id, dn, type, scope, application_id)
  values ((select nvl(max(entity_id), 0) + 1 from a_entity), 'TFA_WS_USER', 'AP', 'A', (select application_id from a_application where name = 'CM-TwoFactor'));

insert into a_grant_key (grant_key_id, grant_key, scope, application_id)
  values ((select nvl(max(grant_key_id), 0) + 1 from a_grant_key), 'view', 'A', (select application_id from a_application where name = 'CM-TwoFactor'));

insert into a_authorization (authorization_id, entity_id, module_id, grant_by, grant_date, grant_start_date, grant_end_date, grant_key_id, status)
  values ((select nvl(max(authorization_id), 0) + 1 from a_authorization), 
    (select entity_id from a_entity where dn = 'TFA_WS_USER' and application_id = (select application_id from a_application where name = 'CM-TwoFactor')),
    (select module_id from a_module where name = 'Opt-in Check' and application_id = (
    (select application_id from a_application where name = 'CM-TwoFactor'))), 'doej', sysdate, sysdate, sysdate + 365, 
    (select grant_key_id from a_grant_key where grant_key = 'view' and application_id = (select application_id from a_application where name = 'CM-TwoFactor')), 'A');

