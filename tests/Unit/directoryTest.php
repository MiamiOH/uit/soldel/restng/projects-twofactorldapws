<?php

namespace MiamiOH\TwoFactorLdapWs\Tests\Unit;

class DirectoryTest extends \MiamiOH\RESTng\Testing\TestCase {

  private $directory;

  private $ldap;
  private $adHandle;
  private $ldapHandle;

  private $handleName = '';
  private $adSearchFilter = '';
  private $adEntry = '';
  private $ldapSearchFilter = '';
  private $ldapEntry = '';

  protected function setUp() {

    $this->handleName = '';
    $this->adSearchFilter = '';
    $this->adEntry = '';
    $this->ldapSearchFilter = '';
    $this->ldapEntry = '';

    $this->adHandle = $this->getMockBuilder('\MiamiOH\RESTng\Connector\LDAP\Connection')
            ->setMethods(array('search','next_entry'))
            ->getMock();
    
    $this->adHandle->method('search')
      ->with($this->callback(array($this, 'adSearchWithFilter')))
      ->will($this->returnCallback(array($this, 'adSearchMock')));
    
    $this->adHandle->method('next_entry')
      ->will($this->returnCallback(array($this, 'adNextEntryMock')));

    $this->ldapHandle = $this->getMockBuilder('\MiamiOH\RESTng\Connector\LDAP\Connection')
            ->setMethods(array('search','next_entry'))
            ->getMock();
    
    $this->ldapHandle->method('search')
      ->with($this->callback(array($this, 'ldapSearchWithOptions')))
      ->will($this->returnCallback(array($this, 'ldapSearchMock')));
    
    $this->ldapHandle->method('next_entry')
      ->will($this->returnCallback(array($this, 'ldapNextEntryMock')));

    // Mocking of the LDAP Service Class
    $this->ldap = $this->getMockBuilder('\MiamiOH\RESTng\Util\LDAP')
            ->setMethods(array('getHandle'))
            ->getMock();

    $this->ldap->method('getHandle')
      ->with($this->callback(array($this, 'getHandleWithName')))
      ->will($this->returnCallback(array($this, 'getHandleMock')));    

    $this->directory = new \MiamiOH\TwoFactorLdapWs\Services\Directory();

    $this->directory->setLdap($this->ldap);
  }

  /**
   * @expectedException Exception
   * @expectedExceptionMessage MiamiOH\TwoFactorLdapWs\Services\Directory::getAdEntry() expects options array
   */
  public function testGetAdEntryRequiresOptions() {

    $entry = $this->directory->getAdEntry();

    $this->assertTrue(is_array($entry));

  }

  public function testGetAdEntry() {

    $entry = $this->directory->getAdEntry(array('dn' => 'cn=group1,ou=groups,dc=it,dc=muohio,dc=edu'));

    $this->assertTrue(is_array($entry));
    $this->assertEquals('cn=group1,ou=groups,dc=it,dc=muohio,dc=edu', $entry['dn']);
    
  }

  public function testGetAdEntryNotFound() {

    $entry = $this->directory->getAdEntry(array('dn' => 'cn=group2,ou=groups,dc=it,dc=muohio,dc=edu'));

    $this->assertTrue(is_array($entry));
    $this->assertEquals(0, count($entry));
    
  }

  /**
   * @expectedException Exception
   * @expectedExceptionMessage MiamiOH\TwoFactorLdapWs\Services\Directory::getAdEntry() expects options array
   */
  public function testGetOpenLDAPEntryRequiresOptions() {

    $entry = $this->directory->getOpenLDAPEntry();

    $this->assertTrue(is_array($entry));

  }

  public function testGetOpenLDAPEntry() {

    $entry = $this->directory->getOpenLDAPEntry(array('uid' => 'member1'));

    $this->assertTrue(is_array($entry));
    $this->assertEquals($this->ldapEntry->attributes['uid'][0], $entry['uid']);
    $this->assertEquals($this->ldapEntry->attributes['givenname'][0], $entry['firstName']);
    $this->assertEquals($this->ldapEntry->attributes['sn'][0], $entry['lastName']);
    $this->assertEquals($this->ldapEntry->attributes['mail'][0], $entry['emailAddress']);
    $this->assertEquals($this->ldapEntry->attributes['muohioedupersontfaenrolled'][0], $entry['enrolled']);
    $this->assertEquals($this->ldapEntry->attributes['muohioedupersontfaoptedin'][0], $entry['optedIn']);
    
  }

  public function testGetOpenLDAPEntryTFADefaults() {

    $entry = $this->directory->getOpenLDAPEntry(array('uid' => 'member5'));

    $this->assertTrue(is_array($entry));
    $this->assertEquals($this->ldapEntry->attributes['uid'][0], $entry['uid']);
    $this->assertEquals($this->ldapEntry->attributes['givenname'][0], $entry['firstName']);
    $this->assertEquals($this->ldapEntry->attributes['sn'][0], $entry['lastName']);
    $this->assertEquals($this->ldapEntry->attributes['mail'][0], $entry['emailAddress']);
    $this->assertEquals('false', $entry['enrolled']);
    $this->assertEquals('false', $entry['optedIn']);
    
  }

  public function testGetOpenLDAPEntryNotFound() {

    $entry = $this->directory->getOpenLDAPEntry(array('uid' => 'member6'));

    $this->assertTrue(is_array($entry));
    $this->assertEquals(0, count($entry));
    
  }


  public function adSearchWithFilter($subject) {
    $this->adSearchFilter = $subject;
    return true;
  }

  public function adSearchMock() {
    $group = new \StdClass();

    switch ($this->adSearchFilter) {
        case 'cn=group1':
            $group->dn = 'cn=group1,ou=groups,dc=it,dc=muohio,dc=edu';
            $group->attributes = array(
              'cn' => 'group1',
              'member' => array(
                    'cn=member1,ou=people,dc=it,dc=muohio,dc=edu',
                    'cn=member2,ou=people,dc=it,dc=muohio,dc=edu',
                    'cn=member3,ou=people,dc=it,dc=muohio,dc=edu',
                    'cn=member4,ou=people,dc=it,dc=muohio,dc=edu',
                )
              );
            break;

        case 'cn=group1004':
            $group->dn = 'cn=group1004,ou=groups,dc=it,dc=muohio,dc=edu';
            $group->attributes = array(
              'cn' => 'group1',
              'member' => ''
              );
            break;

        default:
    }

    $this->adEntry = $group;
  }

  public function adNextEntryMock() {
    return $this->adEntry;
  }

  public function ldapSearchWithOptions($subject) {
    $this->ldapSearchFilter = $subject;
    return true;
  }

  public function ldapSearchMock() {
    $user = new \StdClass();

    switch ($this->ldapSearchFilter) {
        case 'uid=member1':
            $user->dn = 'uid=member1,ou=people,dc=muohio,dc=edu';
            $user->attributes = array(
                'uid' => array('member1'),
                'givenname' => array('Member1'), 
                'sn' => array('Test'), 
                'mail' => array('member1@example.com'), 
                'muohioedupersontfaenrolled' => array('true'), 
                'muohioedupersontfaoptedin' => array('false')
              );
            break;

        case 'uid=member2':
            $user->dn = 'uid=member2,ou=people,dc=muohio,dc=edu';
            $user->attributes = array(
                'uid' => array('member2'),
                'givenname' => array('Member2'), 
                'sn' => array('Test'), 
                'mail' => array('member2@example.com'), 
                'muohioedupersontfaenrolled' => array('true'), 
                'muohioedupersontfaoptedin' => array('false')
              );
            break;

        case 'uid=member3':
            $user->dn = 'uid=member3,ou=people,dc=muohio,dc=edu';
            $user->attributes = array(
                'uid' => array('member3'),
                'givenname' => array('Member3'), 
                'sn' => array('Test'), 
                'mail' => array('member3@example.com'), 
                'muohioedupersontfaenrolled' => array('true'), 
                'muohioedupersontfaoptedin' => array('true')
              );
            break;

        case 'uid=member4':
            $user->dn = 'uid=member4,ou=people,dc=muohio,dc=edu';
            $user->attributes = array(
                'uid' => array('member4'),
                'givenname' => array('Member4'), 
                'sn' => array('Test'), 
                'mail' => array('member4@example.com'), 
                'muohioedupersontfaenrolled' => array('false'), 
                'muohioedupersontfaoptedin' => array('false')
              );
            break;

        case 'uid=member5':
            $user->dn = 'uid=member5,ou=people,dc=muohio,dc=edu';
            $user->attributes = array(
                'uid' => array('member5'),
                'givenname' => array('Member5'), 
                'sn' => array('Test'), 
                'mail' => array('member5@example.com'), 
              );
            break;

        default:
    }

    $this->ldapEntry = $user;
  }

  public function ldapNextEntryMock() {
    return $this->ldapEntry;
  }

  public function getHandleWithName($subject) {
    $this->handleName = $subject;
    return true;
  }

  public function getHandleMock() {
    $handle = '';

    switch ($this->handleName) {
        case 'TFAWS_OpenLDAP':
            return $this->ldapHandle;
            break;

        case 'TFAWS_AD':
            return $this->adHandle;
            break;

    }

    return $handle;
  }

}