<?php
namespace MiamiOH\TwoFactorLdapWs\Tests\Unit;

use MiamiOH\RESTng\App;

class OptInMemberTest extends \MiamiOH\RESTng\Testing\TestCase {

  private $authentication;

  private $directory;

  private $resourceBeingCalledName = '';
  private $resourceBeingCalledArgs = array();
  private $handleName = '';
  private $adEntryOptions = '';
  private $ldapEntryOptions = '';

  protected function setUp() {

    $this->resourceBeingCalledName = '';
    $this->resourceBeingCalledArgs = array();
    $this->handleName = '';
    $this->locationForNameBeingCalled = '';
    $this->locationForParamsBeingCalled = array();
    $this->adEntryOptions = '';
    $this->ldapEntryOptions = '';

    $api = $this->createMock(App::class);

    $api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());
    $api->method('callResource')
      ->with($this->callback(array($this, 'callResourceWithName')), $this->callback(array($this, 'callResourceWithArgs')))
      ->will($this->returnCallback(array($this, 'callResourceMock')));
    $api->method('locationFor')
      ->with($this->callback(array($this, 'locationForNameWith')), $this->callback(array($this, 'locationForParamsWith')))
      ->will($this->returnCallback(array($this, 'locationForMock')));

    $this->directory = $this->getMockBuilder('MiamiOH\TwoFactorLdapWs\Services\Directory')
            ->setMethods(array('getAdEntry', 'getOpenLDAPEntry'))
            ->getMock();

    $this->directory->method('getAdEntry')
      ->with($this->callback(array($this, 'getAdEntryWithOptions')))
      ->will($this->returnCallback(array($this, 'getAdEntryMock')));
      
    $this->directory->method('getOpenLDAPEntry')
      ->with($this->callback(array($this, 'getOpenLDAPEntryWithOptions')))
      ->will($this->returnCallback(array($this, 'getOpenLDAPEntryMock')));
      
    $this->member = new \MiamiOH\TwoFactorLdapWs\Services\Member();

    $this->member->setApp($api);
    $this->member->setDirectory($this->directory);
  }

  /**
   * @expectedException Exception
   * @expectedExceptionMessage You must provide a group id when calling the member service
   */
  public function testGetMemberMissingID() {

    $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

    $request->expects($this->once())->method('getOptions')->willReturn(array());

    $this->member->setRequest($request);

    $resp = $this->member->getMemberList();

  }

  /**
   * @expectedException Exception
   * @expectedExceptionMessage Group with id "1001" was not found
   */
  public function testGetMemberGroupNotFound() {

    $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

    $request->expects($this->once())->method('getOptions')->willReturn(array('id' => 1001));

    $this->member->setRequest($request);

    $resp = $this->member->getMemberList();

  }

  /**
   * @expectedException Exception
   * @expectedExceptionMessage Group with id "1002" has no dn
   */
  public function testGetMemberGroupNoDN() {

    $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

    $request->expects($this->once())->method('getOptions')->willReturn(array('id' => 1002));

    $this->member->setRequest($request);

    $resp = $this->member->getMemberList();

  }

  public function testGetMemberGroupAdEntryNotFound() {

    $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

    $request->expects($this->once())->method('getOptions')->willReturn(array('id' => 1003));

    $this->member->setRequest($request);

    $resp = $this->member->getMemberList();

    $this->assertEquals(\MiamiOH\RESTng\App::API_NOTFOUND, $resp->getStatus());

  }

  /**
   * @expectedException Exception
   * @expectedExceptionMessage AD group "cn=group1004,ou=groups,dc=it,dc=muohio,dc=edu" attribute "member" is not an array
   */
  public function testGetMemberGroupAdEntryNoMemberArray() {

    $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

    $request->expects($this->once())->method('getOptions')->willReturn(array('id' => 1004));

    $this->member->setRequest($request);

    $resp = $this->member->getMemberList();

  }

  public function testGetMemberForGroup() {

    $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

    $request->expects($this->once())->method('getOptions')->willReturn(array('id' => 1));

    $this->member->setRequest($request);

    $resp = $this->member->getMemberList();

    $payload = $resp->getPayload();

    $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
    $this->assertTrue(is_array($payload));

  }

  public function callResourceWithName($subject) {
    $this->resourceBeingCalledName = $subject;
    return true;
  }

  public function callResourceWithArgs($subject) {
    $this->resourceBeingCalledArgs = $subject;
    return true;
  }

  public function callResourceMock() {
    if ($this->resourceBeingCalledName === 'twoFactor.v1.group.id') {
      $response = new \MiamiOH\RESTng\Util\Response();

      $id = $this->resourceBeingCalledArgs['params']['id'];

      switch ($id) {
        case 1001:
            break;

        case 1002:
          $response->setPayload(array(
                    'id' => $id,
                    'dn' => '',
                    'serviceText' => 'Service text for group ' . $id,
                    'rationaleText' => 'Rationale text for group ' . $id,
                    'helpText' => 'Help text for group ' . $id,
                  )
            );
            break;

        case 1003:
          $response->setPayload(array(
                    'id' => $id,
                    'dn' => 'cn=group1003,ou=groups,dc=it,dc=muohio,dc=edu',
                    'serviceText' => 'Service text for group ' . $id,
                    'rationaleText' => 'Rationale text for group ' . $id,
                    'helpText' => 'Help text for group ' . $id,
                  )
            );
            break;

        default:
          $response->setPayload(array(
                    'id' => $id,
                    'dn' => 'cn=group' . $id . ',ou=groups,dc=it,dc=muohio,dc=edu',
                    'serviceText' => 'Service text for group ' . $id,
                    'rationaleText' => 'Rationale text for group ' . $id,
                    'helpText' => 'Help text for group ' . $id,
                  )
            );

      }
      
      return $response;
    }

    return null;
  }

  public function getAdEntryWithOptions($subject) {
    $this->adEntryOptions = $subject;
    return true;
  }

  public function getAdEntryMock() {
    $group = array();

    switch ($this->adEntryOptions['dn']) {
        case 'cn=group1,ou=groups,dc=it,dc=muohio,dc=edu':
            $group['dn'] = 'cn=group1,ou=groups,dc=it,dc=muohio,dc=edu';
            $group['cn'] = 'group1';
            $group['member'] = array(
                    'cn=member1,ou=people,dc=it,dc=muohio,dc=edu',
                    'cn=member2,ou=people,dc=it,dc=muohio,dc=edu',
                    'cn=member3,ou=people,dc=it,dc=muohio,dc=edu',
                    'cn=member4,ou=people,dc=it,dc=muohio,dc=edu',
                );
            break;

        case 'cn=group1004,ou=groups,dc=it,dc=muohio,dc=edu':
            $group['dn'] = 'cn=group1004,ou=groups,dc=it,dc=muohio,dc=edu';
            $group['cn'] = 'group1004';
            $group['member'] = '';
            break;

        default:
    }

    return $group;
  }

  public function getOpenLDAPEntryWithOptions($subject) {
    $this->ldapEntryOptions = $subject;
    return true;
  }

  public function getOpenLDAPEntryMock() {
    $user = array();

    switch ($this->ldapEntryOptions['uid']) {
        case 'member1':
            $user['dn'] = 'uid=member1,ou=people,dc=muohio,dc=edu';
            $user['attributes'] = array(
                'uid' => 'member1',
                'givenName' => 'Member1',
                'sn' => 'Test',
                'mail' => 'member1@example.com',
                'muohioedupersonTFAEnrolled' => 'true',
                'muohioedupersonTFAOptedIn' => 'false'
              );
            break;

        case 'member2':
            $user['dn'] = 'uid=member2,ou=people,dc=muohio,dc=edu';
            $user['attributes'] = array(
                'uid' => 'member2',
                'givenName' => 'Member2',
                'sn' => 'Test',
                'mail' => 'member2@example.com',
                'muohioedupersonTFAEnrolled' => 'true',
                'muohioedupersonTFAOptedIn' => 'false'
              );
            break;

        case 'member3':
            $user['dn'] = 'uid=member3,ou=people,dc=muohio,dc=edu';
            $user['attributes'] = array(
                'uid' => 'member3',
                'givenName' => 'Member3',
                'sn' => 'Test',
                'mail' => 'member3@example.com',
                'muohioedupersonTFAEnrolled' => 'true',
                'muohioedupersonTFAOptedIn' => 'true'
              );
            break;

        case 'member4':
            $user['dn'] = 'uid=member4,ou=people,dc=muohio,dc=edu';
            $user['attributes'] = array(
                'uid' => 'member4',
                'givenName' => 'Member4',
                'sn' => 'Test',
                'mail' => 'member4@example.com',
                'muohioedupersonTFAEnrolled' => 'false',
                'muohioedupersonTFAOptedIn' => 'false'
              );
            break;

        default:
    }

    return $user;
  }

  public function locationForNameWith($subject) {
    $this->locationForNameBeingCalled = $subject;
    return true;
  }

  public function locationForParamsWith($subject) {
    $this->locationForParamsBeingCalled = $subject;
    return true;
  }

  public function locationForMock() {
    $path = str_replace('.', '/', $this->locationForNameBeingCalled);
    $params = $this->locationForParamsBeingCalled;
    $qsFunction = function ($key) use ($params) { return $key . '=' . $params[$key]; };
    $queryString = implode('&', array_map($qsFunction, array_keys($this->locationForParamsBeingCalled)));
    return 'http://example.com/' . $path . ($queryString ? '?' . $queryString : '');
  }

}
