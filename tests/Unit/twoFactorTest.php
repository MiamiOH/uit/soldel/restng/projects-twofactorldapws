<?php
namespace MiamiOH\TwoFactorLdapWs\Tests\Unit;

class TwoFactorTest extends TwoFactorSetup {

  protected function setUp() {
    parent::setup();
  }

  public function testSetLdap() {
    $this->assertEquals($this->ldap_conn, $this->twoFactorObj->getLdap());
  }

}
