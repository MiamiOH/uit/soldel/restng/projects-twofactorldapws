<?php
namespace MiamiOH\TwoFactorLdapWs\Tests\Unit;

use MiamiOH\RESTng\Util\Request;

class TwoFactorPUTTest extends TwoFactorSetup {


  protected function setUp() {
    parent::setup();
    $this->ldap_entry = $this->getMockBuilder('\MiamiOH\RESTng\Connector\LDAP\Entry')
            // ->setMethods(array('attribute_value'))
            ->getMock();
    $this->request = $this->createMock(Request::class);

    $this->ldap_conn->method('search')->will($this->returnCallback(array($this, 'attributeArray')));
    $this->ldap_conn->method('next_entry')->willReturn($this->ldap_entry);
    $this->request->expects($this->once())->method('getResourceParams');
  }

  public function testUpdateLDAP(){

    $this->twoFactorObj->setRequest($this->request);
    $resp = $this->twoFactorObj->updateLDAP();
    // $response = $this->getResponse();
    // $request = $this->getRequest();
    // $params = $request->getResourceParams();
    // $payload = $request->getData();
    // $changes = $this->friendlyToUsableAttributeSwitch($payload);
    // $dn = $this->default_search . '='. $payload['id'].','.$this->container;
    // $success = $this->updateLDAPTFA($dn,$changes);
    //
    // $response->setStatus(API_OK);
    // return $response;

  }
}
