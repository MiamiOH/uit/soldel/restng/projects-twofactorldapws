<?php
namespace MiamiOH\TwoFactorLdapWs\Tests\Unit;

class TwoFactorGETTest extends TwoFactorSetup {

  protected function setUp() {
    parent::setup();

    $this->ldap_entry = $this->getMockBuilder('\MiamiOH\RESTng\Connector\LDAP\Entry')
            // ->setMethods(array('attribute_value'))
            ->getMock();
    $this->request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getResourceParam','getResponse'))
            ->getMock();

    $this->ldap_conn->method('search')->will($this->returnCallback(array($this, 'attributeArray')));
    $this->ldap_conn->method('next_entry')->willReturn($this->ldap_entry);
    $this->request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('id'))
            ->will($this->returnCallback(array($this, 'returnUID')));
  }

  public function testReadFromLDAPTT() {
    $this->enrolled = 'True';
    $this->optedin = 'True';
    $this->readFromLDAP();

  }

  public function testReadFromLDAPTF() {
    $this->enrolled = 'True';
    $this->optedin = 'False';
    $this->readFromLDAP();

  }

  public function testReadFromLDAPFT() {
    $this->enrolled = 'False';
    $this->optedin = 'True';

    $this->readFromLDAP();
  }

  public function testReadFromLDAPFF() {
    $this->enrolled = 'False';
    $this->optedin = 'False';
    $this->readFromLDAP();
  }

public function readFromLDAP() {
  //This is the overall testing code
  $this->enrolled = strtolower($this->enrolled);
  $this->optedin = strtolower($this->optedin);
  $this->twoFactorObj->setRequest($this->request);
  $resp = $this->twoFactorObj->readFromLDAP();
  $payload = $resp->getPayload();
  $this->assertArrayHasKey('id', $payload);
  $this->assertEquals($this->returnValue['uid'],$payload['id']);
  $this->assertEquals($this->enrolled,$payload['enrolled'],'Enrolled');
  $this->assertEquals($this->optedin,$payload['optedIn'],'OptedIn');
}

  public function returnUID() {
    $this->currentUID = 'kidddw';
    return $this->currentUID;
  }
  public function mockGetReturnAttributes() {
    $this->ldapAttributes->attributes = $elements;
    $elements['muohioedupersonTFAEnrolled'] = array($this->enrolled);
    $elements['muohioedupersonTFAOptedIn'] = array($this->optedin);

    return $this->ldapAttributes;
  }

  public function getOptedIn() {
    return $this->optedin;
  }

  public function getEnrolled() {
    return $this->enrolled;
  }


  public function attributeArray() {
    $this->returnValue['uid'] = $this->currentUID;
    $this->returnValue['muohioedupersontfaenrolled'] = array($this->enrolled);
    $this->returnValue['muohioedupersontfaoptedin'] = array($this->optedin);

    // $this->myldap_entry["attributes"] = $this->returnValue;
    $this->myldap_entry["muohioedupersonTFAEnrolled"] = $this->returnValue['muohioedupersonTFAEnrolled'][0];
    $this->myldap_entry["muohioedupersonTFAOptedIn"] = $this->returnValue['muohioedupersonTFAOptedIn'][0];

    $this->ldap_entry->attributes = $this->returnValue;
    // print_r($this->ldap_entry->attributes);

    return $this->myldap_entry;
  }

}
