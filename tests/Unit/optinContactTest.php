<?php
namespace MiamiOH\TwoFactorLdapWs\Tests\Unit;

use MiamiOH\RESTng\App;

class OptInContactTest extends \MiamiOH\RESTng\Testing\TestCase {

  private $contact;

  private $dbh;

  private $resourceBeingCalledName = array();
  private $handleName = '';
  private $ldapEntryOptions = '';

  private $querySql = '';
  private $queryParams = array();

  protected function setUp() {

    $this->resourceBeingCalledName = array();
    $this->handleName = '';
    $this->locationForNameBeingCalled = '';
    $this->locationForParamsBeingCalled = array();
    $this->ldapEntryOptions = '';
    $this->querySql = '';
    $this->queryParams = array();

    $api = $this->createMock(App::class);

    $api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());
    $api->method('callResource')
      ->with($this->callback(array($this, 'callResourceWithName')), $this->callback(array($this, 'callResourceWithArgs')))
      ->will($this->returnCallback(array($this, 'callResourceMock')));

    $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

    $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

    $db->method('getHandle')->willReturn($this->dbh);

    $this->directory = $this->getMockBuilder('MiamiOH\TwoFactorLdapWs\Services\Directory')
            ->setMethods(array('getAdEntry', 'getOpenLDAPEntry'))
            ->getMock();

    $this->directory->method('getAdEntry')
      ->with($this->callback(array($this, 'getAdEntryWithOptions')))
      ->will($this->returnCallback(array($this, 'getAdEntryMock')));
      
    $this->directory->method('getOpenLDAPEntry')
      ->with($this->callback(array($this, 'getOpenLDAPEntryWithOptions')))
      ->will($this->returnCallback(array($this, 'getOpenLDAPEntryMock')));
      
    $this->contact = new \MiamiOH\TwoFactorLdapWs\Services\Contact();

    $this->contact->setLogger();
    $this->contact->setApp($api);
    $this->contact->setDirectory($this->directory);
    $this->contact->setDatabase($db);
  }

  /**
   * @expectedException Exception
   * @expectedExceptionMessage You must provide a group id when calling the contact service
   */
  public function testGetContactMissingID() {

    $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

    $request->expects($this->once())->method('getOptions')->willReturn(array());

    $this->contact->setRequest($request);

    $resp = $this->contact->getContactList();

  }

  /**
   * @expectedException Exception
   * @expectedExceptionMessage Group with id "1001" was not found
   */
  public function testGetContactGroupNotFound() {

    $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

    $request->expects($this->once())->method('getOptions')->willReturn(array('id' => 1001));

    $this->contact->setRequest($request);

    $resp = $this->contact->getContactList();

  }

  public function testGetContactForGroup() {

    $this->dbh->expects($this->once())->method('queryall_array')
      ->with($this->callback(array($this, 'queryall_arrayWithQuery')), $this->callback(array($this, 'queryall_arrayWithParams')))
      ->will($this->returnCallback(array($this, 'queryall_arrayContactMock')));
    
    $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

    $request->expects($this->once())->method('getOptions')->willReturn(array('id' => 1));

    $this->contact->setRequest($request);

    $resp = $this->contact->getContactList();

    $payload = $resp->getPayload();

    $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
    $this->assertTrue(is_array($payload));
    
    $contact = $payload[0];

    $this->assertTrue(is_array($contact));
    $this->assertEquals(5, count(array_keys($contact)));
    $this->assertArrayHasKey('id', $contact);
    $this->assertArrayHasKey('uid', $contact);
    $this->assertArrayHasKey('firstName', $contact);
    $this->assertArrayHasKey('lastName', $contact);
    $this->assertArrayHasKey('emailAddress', $contact);

  }
  

  public function callResourceWithName($subject) {
    $this->resourceBeingCalledName = $subject;
    return true;
  }

  public function callResourceWithArgs($subject) {
    $this->resourceBeingCalledArgs = $subject;
    return true;
  }

  public function callResourceMock() {
    if ($this->resourceBeingCalledName === 'twoFactor.v1.group.id') {
      $response = new \MiamiOH\RESTng\Util\Response();
      
      $id = $this->resourceBeingCalledArgs['params']['id'];

      switch ($id) {
        case 1001: 
            break;

        case 1002:
          $response->setPayload(array(
                    'id' => $id,
                    'dn' => '',
                    'serviceText' => 'Service text for group ' . $id,
                    'rationaleText' => 'Rationale text for group ' . $id,
                    'helpText' => 'Help text for group ' . $id,
                  )            
            );
            break;

        case 1003:
          $response->setPayload(array(
                    'id' => $id,
                    'dn' => 'cn=group1003,ou=groups,dc=it,dc=muohio,dc=edu',
                    'serviceText' => 'Service text for group ' . $id,
                    'rationaleText' => 'Rationale text for group ' . $id,
                    'helpText' => 'Help text for group ' . $id,
                  )            
            );
            break;

        default:
          $response->setPayload(array(
                    'id' => $id,
                    'dn' => 'cn=group' . $id . ',ou=groups,dc=it,dc=muohio,dc=edu',
                    'serviceText' => 'Service text for group ' . $id,
                    'rationaleText' => 'Rationale text for group ' . $id,
                    'helpText' => 'Help text for group ' . $id,
                  )            
            );

      }
      
      return $response;
    }

    return null;
  }

  public function getAdEntryWithOptions($subject) {
    $this->adEntryOptions = $subject;
    return true;
  }

  public function getAdEntryMock() {
    $group = array();

    switch ($this->adEntryOptions['dn']) {
        case 'cn=group1,ou=groups,dc=it,dc=muohio,dc=edu':
            $group['dn'] = 'cn=group1,ou=groups,dc=it,dc=muohio,dc=edu';
            $group['cn'] = 'group1';
            $group['member'] = array(
                    'cn=member1,ou=people,dc=it,dc=muohio,dc=edu',
                    'cn=member2,ou=people,dc=it,dc=muohio,dc=edu',
                    'cn=member3,ou=people,dc=it,dc=muohio,dc=edu',
                    'cn=member4,ou=people,dc=it,dc=muohio,dc=edu',
                );
            break;

        case 'cn=group1004,ou=groups,dc=it,dc=muohio,dc=edu':
            $group['dn'] = 'cn=group1004,ou=groups,dc=it,dc=muohio,dc=edu';
            $group['cn'] = 'group1004';
            $group['member'] = '';
            break;

        default:
    }

    return $group;
  }

  public function getOpenLDAPEntryWithOptions($subject) {
    $this->ldapEntryOptions = $subject;
    return true;
  }

  public function getOpenLDAPEntryMock() {
    $user = array();

    switch ($this->ldapEntryOptions['uid']) {
        case 'member1':
                $user['uid'] = 'member1';
                $user['givenname'] = 'Member1';
                $user['sn'] = 'Test';
                $user['mail'] = 'member1@example.com';
                $user['muohioedupersontfaenrolled'] = 'true';
                $user['muohioedupersontfaoptedin'] = 'false';
            break;

        case 'member2':
                $user['uid'] = 'member2';
                $user['givenname'] = 'Member2';
                $user['sn'] = 'Test';
                $user['mail'] = 'member2@example.com';
                $user['muohioedupersontfaenrolled'] = 'true';
                $user['muohioedupersontfaoptedin'] = 'false';
            break;

        case 'member3':
                $user['uid'] = 'member3';
                $user['givenname'] = 'Member3';
                $user['sn'] = 'Test';
                $user['mail'] = 'member3@example.com';
                $user['muohioedupersontfaenrolled'] = 'true';
                $user['muohioedupersontfaoptedin'] = 'true';
            break;

        case 'member4':
                $user['uid'] = 'member4';
                $user['givenname'] = 'Member4';
                $user['sn'] = 'Test';
                $user['mail'] = 'member4@example.com';
                $user['muohioedupersontfaenrolled'] = 'false';
                $user['muohioedupersontfaoptedin'] = 'false';
            break;

        default:
    }

    return $user;
  }

  public function queryall_arrayWithQuery($subject) {
    $this->querySql = $subject;
    return true;
  }
  
  public function queryall_arrayWithParams($subject) {
    $this->queryParams = $subject;
    return true;
  }

  public function queryall_arrayContactMock() {
    $records = array();

    switch ($this->queryParams) {
      case 1:
        $records = array(
            array(
              'contact_id' => 1,
              'group_id' => 1,
              'contact_uid' => 'member1'
            ),
            array(
              'contact_id' => 2,
              'group_id' => 1,
              'contact_uid' => 'member2'
            ),
          );
    }

    return $records;
  }

}
