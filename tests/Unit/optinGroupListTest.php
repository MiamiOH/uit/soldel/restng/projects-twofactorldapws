<?php
namespace MiamiOH\TwoFactorLdapWs\Tests\Unit;

use MiamiOH\RESTng\App;

class GetGroupListTest extends \MiamiOH\RESTng\Testing\TestCase {
  private $group;

  private $dbh;

  private $resourceBeingCalledName = array();

  private $querySql = '';
  private $queryParams = array();

  protected function setUp() {

    $this->resourceBeingCalledName = array();
    $this->querySql = '';
    $this->queryParams = array();

    $api = $this->createMock(App::class);

    $api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());

    $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array'))
            ->getMock();

    $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

    $db->method('getHandle')->willReturn($this->dbh);

    $this->group = new \MiamiOH\TwoFactorLdapWs\Services\Group();

    $this->group->setApp($api);
    $this->group->setDatabase($db);
  }

  public function testGetGroupList() {

    $this->dbh->expects($this->once())->method('queryall_array')
      ->with($this->callback(array($this, 'queryall_arrayWithQuery')))
      ->will($this->returnCallback(array($this, 'queryall_arrayContactMock')));
    
    $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

    $request->expects($this->once())->method('getOptions')->willReturn(array());

    $this->group->setRequest($request);

    $resp = $this->group->getGroupList();

    $payload = $resp->getPayload();

    $this->assertTrue(is_array($payload));
    $this->assertEquals(2, count($payload));

    $group = $payload[0];

    $this->assertTrue(is_array($group));
    $this->assertEquals(5, count($group));
    $this->assertArrayHasKey('id', $group);
    $this->assertArrayHasKey('dn', $group);
    $this->assertArrayHasKey('serviceText', $group);
    $this->assertArrayHasKey('rationaleText', $group);
    $this->assertArrayHasKey('helpText', $group);
  }

  public function queryall_arrayWithQuery($subject) {
    $this->querySql = $subject;
    return true;
  }
  
  public function queryall_arrayWithParams($subject) {
    $this->queryParams = $subject;
    return true;
  }

  public function queryall_arrayContactMock() {
    $records = array(
        array(
          'id' => 1,
          'dn' => 'cn=group1,ou=groups,dc=it,dc=muohio,dc=edu',
          'service_text' => '',
          'rationale_text' => '',
          'help_text' => '',
        ),
        array(
          'id' => 2,
          'dn' => 'cn=group2,ou=groups,dc=it,dc=muohio,dc=edu',
          'service_text' => '',
          'rationale_text' => '',
          'help_text' => '',
        ),
      );

    return $records;
  }

}