<?php
namespace MiamiOH\TwoFactorLdapWs\Tests\Unit;

use MiamiOH\RESTng\App;

class GetGroupTest extends \MiamiOH\RESTng\Testing\TestCase {

  protected function setUp() {

    $api = $this->createMock(App::class);

    $api->method('newResponse')->willReturn(new \MiamiOH\RESTng\Util\Response());
    $api->method('callResource')
      ->with($this->callback(array($this, 'callResourceWithName')), $this->callback(array($this, 'callResourceWithArgs')))
      ->will($this->returnCallback(array($this, 'callResourceMock')));
    $api->method('locationFor')
      ->with($this->callback(array($this, 'locationForNameWith')), $this->callback(array($this, 'locationForParamsWith')))
      ->will($this->returnCallback(array($this, 'locationForMock')));

    // Mocking the DB Connecting
    $this->dbh = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database\DBH')
            ->setMethods(array('queryall_array','queryfirstrow_assoc'))
            ->getMock();

    $this->dbh->error_string = '';

    $db = $this->getMockBuilder('\MiamiOH\RESTng\Connector\Database')
            ->setMethods(array('getHandle'))
            ->getMock();

    $db->method('getHandle')->willReturn($this->dbh);


    $this->group = new \MiamiOH\TwoFactorLdapWs\Services\Group();

    $this->group->setApp($api);
    $this->group->setDatabase($db);

  }


  /**
   * @expectedException Exception
   * @expectedExceptionMessage You must provide a group id when calling the group service
   */
  public function testGetGroupMissingID() {

    $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions'))
            ->getMock();

    $this->group->setRequest($request);

    $resp = $this->group->getGroup();

  }

  public function testGroupNotFound() {

    $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions','getResourceParam'))
            ->getMock();
    $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('id'))
            ->willReturn(123);

    $this->dbh->method('queryfirstrow_assoc')->willReturn(\MiamiOH\RESTng\Core\DB\DBH::DB_EMPTY_SET);

    $this->group->setRequest($request);

    $resp = $this->group->getGroup();
    $this->assertEquals(\MiamiOH\RESTng\App::API_NOTFOUND, $resp->getStatus());

  }
  /**
   * @expectedException Exception
   * @expectedExceptionMessage  Group record for id "234" missing DN value
   */
  public function testGetGroupWithNoDN() {
    $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions','getResourceParam'))
            ->getMock();
    $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('id'))
            ->willReturn(234);
    $recordSet = array(
      'id' => '234',
      'dn' => '',
      'serviceText' => 'Service text for group ' . '234',
      'rationaleText' => 'Rationale text for group ' . '234',
      'helpText' => 'Help text for group ' . '234',
    );

    $this->dbh->method('queryfirstrow_assoc')->willReturn($recordSet);

    $this->group->setRequest($request);

    $resp = $this->group->getGroup();

  }


  public function testSuccessfulGet() {
    $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
            ->setMethods(array('getOptions','getResourceParam'))
            ->getMock();
    $request->expects($this->once())->method('getResourceParam')
            ->with($this->equalTo('id'))
            ->willReturn(345);
    $recordSet = array(
      'id' => '345',
      'dn' => 'cn=group345,ou=groups,dc=it,dc=muohio,dc=edu',
      'serviceText' => 'Service text for group ' . '345',
      'rationaleText' => 'Rationale text for group ' . '345',
      'helpText' => 'Help text for group ' . '345',
    );

    $this->dbh->method('queryfirstrow_assoc')->willReturn($recordSet);

    $this->group->setRequest($request);

    $resp = $this->group->getGroup();

    $payload = $resp->getPayload();
    $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
    $this->assertTrue(is_array($payload));
    $this->assertEquals($payload,$recordSet);
  }

  public function testSuccessfulGetWithMissingServiceText() {
      $request = $this->getMockBuilder('\MiamiOH\RESTng\Util\Request')
              ->setMethods(array('getOptions','getResourceParam'))
              ->getMock();
      $request->expects($this->once())->method('getResourceParam')
              ->with($this->equalTo('id'))
              ->willReturn(456);
      $recordSet = array(
        'id' => '456',
        'dn' => 'cn=group456,ou=groups,dc=it,dc=muohio,dc=edu',
        'serviceText' => '',
        'rationaleText' => 'Rationale text for group ' . '456',
        'helpText' => 'Help text for group ' . '456',
      );

      $this->dbh->method('queryfirstrow_assoc')->willReturn($recordSet);

      $this->group->setRequest($request);

      $resp = $this->group->getGroup();

      $payload = $resp->getPayload();
      $this->assertEquals(\MiamiOH\RESTng\App::API_OK, $resp->getStatus());
      $this->assertTrue(is_array($payload));
      $this->assertEquals($payload,$recordSet);

    }


  public function callResourceWithName($subject) {
    $this->resourceBeingCalledName = $subject;
    return true;
  }

  public function callResourceWithArgs($subject) {
    $this->resourceBeingCalledArgs = $subject;
    return true;
  }

  public function locationForNameWith($subject) {
    $this->locationForNameBeingCalled = $subject;
    return true;
  }

  public function locationForParamsWith($subject) {
    $this->locationForParamsBeingCalled = $subject;
    return true;
  }


}
