<?php
namespace MiamiOH\TwoFactorLdapWs\Tests\Unit;

use MiamiOH\RESTng\App;
use MiamiOH\RESTng\Testing\TestCase;
use MiamiOH\RESTng\Util\Response;
use MiamiOH\TwoFactorLdapWs\Services\TwoFactor;

abstract class TwoFactorSetup extends TestCase {

  protected $twoFactorObj;
  protected $ldap;
  protected $ldap_conn;
  protected $ldap_entry;
  protected $myldap_entry;
  protected $request;



  protected function setUp() {
    /*
        The API service handles a lot of the coordination between the consumer
        interface layer and the business service layer. We mock that here
        so that we have complete control of what happens.
    */
    $api = $this->createMock(App::class);

    /*
        We will use the real Response class in this case. If we wanted to enforce
        specific behaviors of the response, we could mock it as well.
    */
    $api->method('newResponse')->willReturn(new Response());

    //Setting up the Mocking of the LDAP\Connection
    $this->ldap_conn = $this->getMockBuilder('\RESTng\Connector\LDAP\Connection')
            ->setMethods(array('search','next_entry'))
            ->getMock();

    // Mocking of the LDAP Service Class
    $this->ldap = $this->getMockBuilder('\RESTng\Util\LDAP')
            ->setMethods(array('getHandle'))
            ->getMock();

    $this->ldap->method('getHandle')->willReturn($this->ldap_conn);

    // Creating the TwoFactorObject that is to be tested
    $this->twoFactorObj = new TwoFactor();

    /*
    Inject the mock API service using the RESTng\Service::setApi method. Since
    all business services are supposed to inherit from that class, this method
    should exist.
    */
    $this->twoFactorObj->setApp($api);
    $this->twoFactorObj->setLdap($this->ldap);
  }

}
