insert into tfa_optin_group (group_id, group_dn, service_text, rationale_text, help_text)
    values (tfa_optin_group_id.nextval, 'CN=duit-tfateam,OU=uit,OU=manual,OU=Groups,DC=it,DC=muohio,DC=edu',
        'Authentication Service',
        'This is an important system and gives access to restricted data.',
        'Call ITHelp.');
