
insert into cm_config (config_application, config_key, 
        config_data_type, config_data_structure, config_category, 
        config_desc, config_value, activity_date, activity_user)
    values ('TwoFactor', 'messageTemplate', 
        'text', 'scalar', 'Opt-in Check',
        'Template used for email to user', '{firstName} {lastName}
CC: {contactList}

You have been identified as a having access to {serviceText} in a role which has access to privileged information. As such, you are required to opt-in to the additional two factor service.

{rationaleText}

{helpText}',
        sysdate, 'doej');

insert into cm_config (config_application, config_key, 
        config_data_type, config_data_structure, config_category, 
        config_desc, config_value, activity_date, activity_user)
    values ('TwoFactor', 'messageSubject', 
        'text', 'scalar', 'Opt-in Check',
        'Template used for email subject', 'TFA Opt-In Required',
        sysdate, 'doej');

insert into cm_config (config_application, config_key, 
        config_data_type, config_data_structure, config_category, 
        config_desc, config_value, activity_date, activity_user)
    values ('TwoFactor', 'messageFrom', 
        'text', 'scalar', 'Opt-in Check',
        'From address used for email', 'ITHelp <ithelp@miamioh.edu>',
        sysdate, 'doej');
    