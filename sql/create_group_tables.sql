create table tfa_optin_group (
    group_id int primary key,
    group_dn varchar2(256),
    service_text varchar2(2048),
    rationale_text varchar2(2048),
    help_text varchar2(2048)
);

create table tfa_optin_group_contact (
    contact_id int primary key,
    group_id int,
    contact_uid varchar2(16)
);

create sequence tfa_optin_group_id start with 0 increment by 1 minvalue 0 nocache nocycle noorder;
create sequence tfa_optin_group_contact_id start with 0 increment by 1 minvalue 0 nocache nocycle noorder;